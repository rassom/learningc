﻿#include <stdio.h>
#include <stdlib.h> // Must-have library for malloc to work


double* read(char const* filename, int* size) 
{
    int i = 0;
    FILE* fin = fopen(filename, "r");
    
    // Read first number from the file to the 'size' variable.
    fscanf(fin, "%d", size);

    double *arr = (double*) malloc(sizeof(double) * (*size));
    
    // Read while it's not end of the file and i less than size.
    while (!feof(fin) && i < (*size))
    {
        fscanf(fin, "%lf", &arr[i]);
        i++;
    }

    if (i != (*size)){
        *size = 0;
    }

    fclose(fin);
    return arr;
}

double* solve(double* arr, int size)
{
    double temp; // temp stands for a temporary variable
    
    for (int i = 1; i < size; i++)
    {
        if (arr[i] >= 0 && arr[i-1] < 0){
            int j = i;        

            while (j > 0 && arr[j - 1] < 0) {
                temp = arr[j];
                arr[j] = arr[j - 1];
                arr[j - 1] = temp;
                j--;
            }
        }
    }

    return arr;
}

void write(const char* filename, double* arr, int size)
{
    FILE* fout = fopen(filename, "w");

    fprintf(fout, "%d ", size);
    
    for (int i = 0; i < size; i++)
    {
        fprintf(fout, "%lf ", arr[i]);   
    }

    free(arr); 
    fclose(fout);
}

int main(int argc, char const *argv[])
{
    if (argc < 3) {
        printf("Please indicate input and output files!\n");
        return 0;
    }

    int size;
    char const* inputFileName = argv[1];
    char const* outputFileName = argv[2];

    double* arr = read(inputFileName, &size);

    if (size <= 0) {
        FILE* fout = fopen(outputFileName, "w");
        fprintf(fout, "%d ", 0);
        fclose(fout);
        return 0;
    }
    
    arr = solve(arr, size);

    write(outputFileName, arr, size);
    return 0;
}