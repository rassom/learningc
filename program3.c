﻿#include <stdio.h>
#include <stdlib.h>

double* read(char const* filename, int* size) 
{
    int i = 0;
    FILE* fin = fopen(filename, "r");
    
    fscanf(fin, "%d", size);

    double *arr = (double*) malloc(sizeof(double) * (*size));
    
    while (!feof(fin) && i < (*size))
    {
        fscanf(fin, "%lf", &arr[i]);
        i++;
    }

    if (i != (*size)){
        *size = 0;
    }

    fclose(fin);
    return arr;
}

double* solve(double* arr, int size)
{
    double old = arr[0], newOld;

    for (int i = 1; i < size - 1; i++)
    {
        newOld = arr[i];
        arr[i] = 0.5 * (old + arr[i+1]);
        old = newOld;
    }
    
    return arr;
}

void write(const char* filename, double* arr, int size)
{
    FILE* fout = fopen(filename, "w");

    fprintf(fout, "%d ", size);
    
    for (int i = 0; i < size; i++)
    {
        fprintf(fout, "%lf ", arr[i]);   
    }

    free(arr); 
    fclose(fout);
}

int main(int argc, char* argv[])
{
    if (argc < 3) {
        printf("Please indicate input and output files!\n");
        return 0;
    }

    int size;

    double* arr = read(argv[1], &size);
    
    if (size <= 0) {
        FILE* fout = fopen(argv[2], "w");
        fprintf(fout, "%d ", 0);
        fclose(fout);
        return 0;
    }

    arr = solve(arr, size);
    
    write(argv[2], arr, size);

    return 0;
}
