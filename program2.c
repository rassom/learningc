﻿#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void read(char const* filename, unsigned long long int* n, int* p, int* b) {
	FILE* fin = fopen(filename, "r");
	fscanf(fin, "%llu", n);
	fscanf(fin, "%d", p);
	fscanf(fin, "%d", b);
	fclose(fin);
}

void write(char const* filename, unsigned long long int n) {
	FILE* fout = fopen(filename, "w");
    fprintf(fout, "%llu ", n);
	fclose(fout);
}

unsigned long long int solve(unsigned long long int n, int p, int b){
	char* arr = (char*) malloc (sizeof(long) * 8);
	unsigned long long int i;
	int index = 63;
	
	for (i = 1UL << 63; i > 0; i = i / 2){
		if (n & i)
			arr[index] = '1';
		else 
			arr[index] = '0';

		// printf("%c", arr[index]);
		index--;
	}

	// Check if position and bit are valid. If not then return 0, so 
	// 0 is written the output file.
	if (p < 0 || b < 0 || b > 1) {
	  return 0;
	} else {
	  arr[p] = (b + '0');
	}
	
	n = 0;
	// printf("\n");
	for (int j = 63; j >= 0; j--){
		n = n * 2 + (arr[j] - '0');
		// printf("%c", arr[j]);
	}

	return n;
}

int main(int argc, char* argv[])
{ 
	if (argc < 3) {
        printf("Please indicate input and output files!\n");
        return 0;
    }

	unsigned long long int num;
	int pose;
	int bit;
	read(argv[1], &num, &pose, &bit);

	unsigned long long int result = solve(num, pose, bit);

	write(argv[2], result);
	
	return 0;
}