# Instructions

## How to compile your code

```c
gcc program.c -o program
```

## How to run your code

Note: Make sure you provide to file names

```c
./program input.txt output.txt
```
